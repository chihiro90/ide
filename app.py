import dash


# define css
external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)
app.title = 'IDE'
server = app.server
app.config.suppress_callback_exceptions = True
app.scripts.config.serve_locally = True
