import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output

from app import app
from apps import upload_csv, by_gender, by_state, weather, per_agegroup, map, about

app.css.append_css({
    'external_url': 'https://codepen.io/chriddyp/pen/bWLwgP.css'
})

app.layout = html.Div([
    html.Div([
        html.Div([
            html.Div(
                dcc.Link('Cancer Statistics by Gender', href='/apps/by_gender'),
                style={'display': 'inline-block', 'align': 'center'},
            ),
            html.Div(
                dcc.Link('Cancer Statistics by State', href='/apps/by_state'),
                style={'display': 'inline-block'},
            ),
            html.Div(
                dcc.Link('Cancer Statistics per Age Group', href='/apps/per_agegroup'),
                style={'display': 'inline-block'},
            ),
            html.Div(
                dcc.Link('Weather Statistics', href='/apps/weather'),
                style={'display': 'inline-block'},
            ),
            html.Div(
                dcc.Link('Weather & Cancer Maps', href='/apps/map', className="nav-content"),
                style={'display': 'inline-block'},
            ),
            html.Div(
                dcc.Link('About', href='/apps/about', className="nav-content"),
                style={'display': 'inline-block'},
            ),

        ], className="horizontal-menu"),
    ], className="row"),
    dcc.Location(id='url', refresh=False),
    html.Div(id='page-content'),
])


@app.callback(Output('page-content', 'children'),
              [Input('url', 'pathname')])
def display_page(pathname):
    if pathname == '/apps/upload_csv':
        return upload_csv.layout
    elif pathname == '/apps/by_gender':
        return by_gender.layout
    elif pathname == '/apps/by_state':
        return by_state.layout
    elif pathname == '/apps/per_agegroup':
        return per_agegroup.layout
    elif pathname == '/apps/weather':
        return weather.layout
    elif pathname == '/apps/map':
        return map.layout
    elif pathname == '/apps/about':
        return about.layout
    else:
        return map.layout


if __name__ == '__main__':
    app.run_server(debug=True)
