'''
Weather Stats Per State - Line Graph.
Allow end-user to select weather state from the dropdown list
and year range from the range slider.
Then show the line graph of selected weather state info per state during the specific selected year range
'''
import dash
import dash_core_components as dcc
import dash_html_components as html

import plotly.graph_objs as go
import pandas as pd

from app import app

# define polygon data for state boundaries are collected from data.gov.au website
# https://data.gov.au/organization/department-of-industry?q=NT&sort=score+desc%2C+metadata_modified+desc
NT_JSON = 'https://data.gov.au/geoserver/nt-state-boundary-psma-administrative-boundaries/wfs?request=GetFeature&typeName=ckan_5162e11c_3259_4894_8b9e_f44540b6cb11&outputFormat=json'
NSW_JSON = 'https://data.gov.au/geoserver/nsw-state-boundary/wfs?request=GetFeature&typeName=ckan_a1b278b1_59ef_4dea_8468_50eb09967f18&outputFormat=json'
TAS_JSON = 'https://data.gov.au/geoserver/tas-state-boundary/wfs?request=GetFeature&typeName=ckan_cf2ebc53_1633_4c5c_b892_bfc3945d913b&outputFormat=json'
VIC_JSON = 'https://data.gov.au/geoserver/vic-state-boundary-psma-administrative-boundaries/wfs?request=GetFeature&typeName=ckan_b90c2a19_d978_4e14_bb15_1114b46464fb&outputFormat=json'
SA_JSON = 'https://data.gov.au/geoserver/sa-state-boundary-psma-administrative-boundaries/wfs?request=GetFeature&typeName=ckan_8f996b8c_d939_4757_a231_3fec8cb8e929&outputFormat=json'
QLD_JSON = 'https://data.gov.au/geoserver/qld-state-boundary-psma-administrative-boundaries/wfs?request=GetFeature&typeName=ckan_2dbbec1a_99a2_4ee5_8806_53bc41d038a7&outputFormat=json'
WA_JSON = 'https://data.gov.au/geoserver/wa-state-boundary-psma-administrative-boundaries/wfs?request=GetFeature&typeName=ckan_5c00d495_21ba_452d_ae46_1ad0ca05e41f&outputFormat=json'

# define css
colors = {
    'background': '#ffffff',
    'text': '#000000',
    'component': '#BDC3C7',
}
component_style = {
            'width': '97%',
            'padding': '0px 0px 0px 0px',
            'display': 'block',
            'margin': '0 auto'
}

app.css.append_css({
    'external_url': 'https://codepen.io/chriddyp/pen/bWLwgP.css'
})


# define mappers
cancer_stat_mappers = {
    'incident_count': 'Incident Count',
    'i_asr': 'Incident Crude Rate',
    'mortality_count': 'Mortality Count',
    'm_asr': 'Mortality Crude Rate',
}

weather_stat_mappers = {
    'tmean': 'Temperature - Mean',
    'tmin': 'Temperature - Min',
    'tmax': 'Temperature - Max',
    'rainfall': 'Rainfall',
    'evaporation': 'Evaporation',
    'cloud': 'Cloud',
}

cancer_type_mappers = {
    'LIP': 'Lip',
    'MOUTH': 'Mouth',
    'THYROID': 'Thyroid',
    'TONGUE': 'Tongue',
    'OESOPHAGEAL': 'Oesophageal',
}


state_latlong_mappers = {
    'New South Wales': {'lat': -31.2532, 'lon': 146.9211},
    'Victoria': {'lat': -37.4713, 'lon': 144.7852},
    'Queensland': {'lat': -20.9176, 'lon': 142.7028},
    'Western Australia': {'lat': -27.6728, 'lon': 121.6283},
    'South Australia': {'lat': -30.0002, 'lon': 136.2092},
    'Tasmania': {'lat': -41.4545, 'lon': 145.9707},
    'Australian Capital Territory': {'lat': -35.4735, 'lon': 149.0124},
    'Northern Territory': {'lat': -19.4914, 'lon': 132.5510},
}

station_mappers = {
    'NSW': 'New South Wales',
    'VIC': 'Victoria',
    'QLD': 'Queensland',
    'WA': 'Western Australia',
    'SA': 'South Australia',
    'TAS': 'Tasmania',
    'ACT': 'Australia Capital Territory',
    'NT': 'Northen Territory',
}

# the temperature mapping is inspired by bom website
# http://www.bom.gov.au/jsp/ncc/climate_averages/temperature/index.jsp
temp_maps = {
    range(-100, -3): 'rgb(4,90,141)',
    range(-3, 0): 'rgb(5,112,176)',
    range(0, 3): 'rgb(54,144,192)',
    range(3, 6): 'rgb(116,169,207)',
    range(6, 9): 'rgb(166,189,219)',
    range(9, 12): 'rgb(208,209,230)',
    range(12, 15): 'rgb(236,231,242)',
    range(15, 18): 'rgb(255,255,251)',
    range(18, 21): 'rgb(255,255,229)',
    range(21, 24): 'rgb(255,247,188)',
    range(24, 27): 'rgb(254,227,145)',
    range(27, 30): 'rgb(254,196,79)',
    range(27, 33): 'rgb(254,153,41)',
    range(33, 36): 'rgb(236,112,20)',
    range(36, 39): 'rgb(204,76,2)',
    range(39, 100): 'rgb(153,52,4)',
}

temperature_scl = [
    [0.0, 'rgb(4,90,141)'],
    [0.2, 'rgb(5,112,176)'],
    [0.4, 'rgb(54,144,192)'],
    [0.6, 'rgb(116,169,207)'],
    [0.8, 'rgb(166,189,219)'],
    [1.0, 'rgb(208,209,230)'],
    [1.2, 'rgb(236,231,242)'],
    [1.4, 'rgb(255,255,251)'],
    [1.6, 'rgb(255,255,229)'],
    [1.8, 'rgb(255,247,188)'],
    [2.0, 'rgb(254,227,145)'],
    [2.2, 'rgb(254,196,79)'],
    [2.4, 'rgb(254,153,41)'],
    [2.6, 'rgb(236,112,20)'],
    [2.8, 'rgb(204,76,2)'],
    [3.0, 'rgb(153,52,4)'],
]


# read csv using pandas
df_weather = pd.read_csv('fixtures/weather.csv')
df_weather = df_weather[(df_weather.year >= 1971) & (df_weather.year <= 2015)]


df_cancer = pd.read_csv('fixtures/cancer/cancer_state.csv')
df_cancer = df_cancer[(df_cancer.year >= 1971) & (df_cancer.year <= 2015)]

# define the token needed for loading the map
token = 'pk.eyJ1IjoiYWRpdHlhYW5hbHl0aWNzIiwiYSI6ImNqbG45bTE0eDFnd2wzd3M2MXpyem45c3MifQ.IMY_zeUjj3zdW-XM9fU_Nw'


# use dash layout to define dash components
# https://dash.plot.ly/getting-started

layout = html.Div(style={'backgroundColor': colors['background']}, children=[
    html.Div([
        # weather heatmap on the left side
        html.Div([
            html.Label('Select Weather Stat:', style=component_style),
            dcc.Dropdown(
                id='weather-stat',
                options=[
                    {'label': weather_stat_mappers['tmean'], 'value': 'tmean'},
                    {'label': weather_stat_mappers['tmin'], 'value': 'tmin'},
                    {'label': weather_stat_mappers['tmax'], 'value': 'tmax'},
                    {'label': weather_stat_mappers['rainfall'], 'value': 'rainfall'},
                    {'label': weather_stat_mappers['evaporation'], 'value': 'evaporation'},
                    {'label': weather_stat_mappers['cloud'], 'value': 'cloud'},
                ], style={'margin-right': '10%'},
                value='tmean',
                multi=False,
            ),
            dcc.Graph(id='output-weather', style={'width': '100%', 'height': '650px', 'padding': '0px 0px 0px 0px'}),
        ], style={'width': '48%', 'display': 'inline-block', 'height': '80%'}),

        # cancer cluster map on the right side
        html.Div([
            # cancer stat input
            # html.Label('Select Cancer Stat:', style=component_style),
            dcc.Dropdown(
                id='cancer-stat',
                options=[
                    {'label': cancer_stat_mappers['incident_count'], 'value': 'incident_count'},
                    {'label': cancer_stat_mappers['i_asr'], 'value': 'i_asr'},
                    {'label': cancer_stat_mappers['mortality_count'], 'value': 'mortality_count'},
                    {'label': cancer_stat_mappers['m_asr'], 'value': 'm_asr'},
                ],
                value='incident_count',
                multi=False,
            ),
            # cancer type input
            # html.Label('Select Cancer Type/s:', style=component_style),
            dcc.Checklist(
                id='cancer-type',
                options=[
                    {'label': 'Lip', 'value': 'LIP'},
                    {'label': 'Mouth', 'value': 'MOUTH'},
                    {'label': 'Thyroid', 'value': 'THYROID'},
                    {'label': 'Tongue', 'value': 'TONGUE'},
                    {'label': 'Oesophageal', 'value': 'OESOPHAGEAL'},
                ],
                values=['LIP', 'MOUTH', 'THYROID', 'TONGUE', 'OESOPHAGEAL'],
                labelStyle={'display': 'inline-block'},
                style=component_style,
            ),
            # Gender Input
            # html.Label('Select Gender/s:', style=component_style),
            dcc.RadioItems(
                id='gender',
                options=[
                    {'label': 'Male', 'value': 'M'},
                    {'label': 'Female', 'value': 'F'},
                    {'label': 'Person (M+F)', 'value': 'P'},
                ],
                value='P',
                style=component_style,
                labelStyle={'display': 'inline-block'},
            ),
            dcc.Graph(id='output-cancer', style={'width': '100%', 'height': '650px', 'padding': '0px 0px 0px 0px'}),
        ], style={'width': '48%', 'display': 'inline-block', 'height': '80%'}),
        # year range input
        dcc.RangeSlider(
            id='year--slider',
            min=df_weather['year'].min(),
            max=df_weather['year'].max(),
            value=[df_weather['year'].max()-10, df_weather['year'].max()],
            step=0.5,
            marks={str(year): '{}'.format(year) for year in df_weather['year'].unique()}
        ),
    ], style=component_style),
])


# weather map
@app.callback(
   dash.dependencies.Output('output-weather', 'figure'),
   [dash.dependencies.Input('weather-stat', 'value'),
    dash.dependencies.Input('year--slider', 'value')])
def callback_weather(weather_stat_name, year_value):
    import plotly.graph_objs as graph_objs
    # draw heatmat
    # extract lat lon and the input weather stat from weather file
    filtered_df_weather = df_weather[
        ((df_weather['year'] >= year_value[0]) & (df_weather['year'] <= year_value[1]))
        & (df_weather[weather_stat_name].notnull())
        & (df_weather[weather_stat_name] != 99999.9)
    ]

    filtered_df_weather[[weather_stat_name]] = filtered_df_weather[[weather_stat_name]].astype(float)
    grouped = filtered_df_weather.groupby(['state_code']).aggregate({weather_stat_name: 'mean'}).to_dict()

    data = graph_objs.Data([
        graph_objs.Scattermapbox(
            lat=['-27.2744'],
            lon=['133.7751'],
            mode='markers',
        )
    ])
    # Todo: fix this when you have time
    # inset = [
    #     go.Scattermapbox(
    #         lat=['-27.2744'],
    #         lon=['133.7751'],
    #         mode='markers',
    #     ),
    #     go.Choropleth(
    #         colorscale=temperature_scl,
    #         autocolorscale=False,
    #         locations=pd.Series(filtered_df_weather['state_code'].unique()),
    #         z=grouped[weather_stat_name],
    #         locationmode='country names',
    #         text=pd.Series(filtered_df_weather['state_code'].unique()),
    #         marker=dict(
    #             line=dict(
    #                 color='rgb(255,255,255)',
    #                 width=2
    #             )),
    #         colorbar=dict(
    #             title=weather_stat_mappers[weather_stat_name])
    #         )]

    layout = go.Layout(
        autosize=True,
        hovermode='closest',
        mapbox=dict(
            layers=[
                dict(
                    sourcetype='geojson',
                    source=NT_JSON,
                    type='fill',
                    color=colour_lookup(int(grouped[weather_stat_name]['NT']))
                ),
                dict(
                    sourcetype = 'geojson',
                    source=NSW_JSON,
                    type='fill',
                    color=colour_lookup(int(grouped[weather_stat_name]['NSW']))
                ),
                dict(
                    sourcetype = 'geojson',
                    source=TAS_JSON,
                    type='fill',
                    color=colour_lookup(int(grouped[weather_stat_name]['TAS']))
                ),
                dict(
                    sourcetype='geojson',
                    source=VIC_JSON,
                    type='fill',
                    color=colour_lookup(int(grouped[weather_stat_name]['VIC']))
                ),
                dict(
                    sourcetype='geojson',
                    source=SA_JSON,
                    type='fill',
                    color=colour_lookup(int(grouped[weather_stat_name]['SA']))
                ),
                dict(
                    sourcetype='geojson',
                    source=QLD_JSON,
                    type='fill',
                    color=colour_lookup(int(grouped[weather_stat_name]['QLD']))
                ),
                dict(
                    sourcetype = 'geojson',
                    source=WA_JSON,
                    type='fill',
                    color=colour_lookup(int(grouped[weather_stat_name]['WA']))
                ),
                # TODO: find the polygon data for ACT and figure out why there is no weather data set for ACT in weather csv file
                # dict(
                #     sourcetype='geojson',
                #     source='https://data.gov.au/geoserver/wa-state-boundary-psma-administrative-boundaries/wfs?request=GetFeature&typeName=ckan_5c00d495_21ba_452d_ae46_1ad0ca05e41f&outputFormat=json',
                #     type='fill',
                #     color=colour_lookup(int(grouped[weather_stat_name]['ACT']))
                # ),
            ],
            accesstoken=token,
            bearing=0,
            # australia center
            center=dict(
                lat=-27.2744,
                lon=133.7751
            ),
            pitch=0,
            zoom=3,
            style='light',
        ),
    )

    return dict(data=data, layout=layout)


# cancer map
@app.callback(
   dash.dependencies.Output('output-cancer', 'figure'),
   [dash.dependencies.Input('cancer-stat', 'value'),
    dash.dependencies.Input('cancer-type', 'values'),
    dash.dependencies.Input('gender', 'value'),
    dash.dependencies.Input('year--slider', 'value')])
def callback_cancer(cancer_stat_name, cancer_type_values, gender_value, year_value):
    '''
    use scattermapbox to draw the cancer clusters for each state
    https://plot.ly/python/scattermapbox/
    use heatmaps to show the rainfall millimetres for each station that belongs to state
    https://plot.ly/python/heatmaps/
    '''

    # get the incident count from cancer per state file
    filtered_df_cancer = df_cancer[
        ((df_cancer.year >= year_value[0]) & (df_cancer.year <= year_value[1]))
        & (df_cancer[cancer_stat_name].notnull())
        & (df_cancer[cancer_stat_name] != 99999.9)
        & (df_cancer[cancer_stat_name] != '..')
        & (df_cancer[cancer_stat_name] != 'n.p.')
        & (df_cancer['cancer_type'].isin(cancer_type_values))
        & (df_cancer['gender'] == gender_value)
    ]
    filtered_df_cancer[[cancer_stat_name]] = filtered_df_cancer[[cancer_stat_name]].astype(float)
    filtered_df_cancer[['gender']] = filtered_df_cancer[['gender']].astype(str)

    traces = []

    # depends on cancer stat, total will be sum or mean
    if cancer_stat_name in ('incident_count', 'mortality_count'):
        total = filtered_df_cancer[cancer_stat_name].sum()
    else:
        total = filtered_df_cancer[cancer_stat_name].mean()

    for state in filtered_df_cancer.state.unique():
        state_total = filtered_df_cancer[filtered_df_cancer['state'] == state][cancer_stat_name].sum()
        if cancer_stat_name in ('incident_count', 'mortality_count'):
            # state_total = 0
            text = state + ': ' + str(state_total) + '<br />'
            cancer_per_state = filtered_df_cancer[filtered_df_cancer['state'] == state].groupby(['cancer_type']).aggregate({cancer_stat_name: 'sum'})[cancer_stat_name]
            for k, v in cancer_per_state.items():
                text = text + cancer_type_mappers.get(k, k) + ': ' + str(v) + '<br />'
                # state_total = state_total + int(v)

            # total = total + state_total
            size = 0 if total == 0 else state_total/total*70

            traces.append(go.Scattermapbox(
                lat=[state_latlong_mappers.get(state)['lat']],
                lon=[state_latlong_mappers.get(state)['lon']],
                mode='markers',
                marker=dict(
                    size=size,
                    color='rgb(255, 0, 0)',
                    # opacity=1.0
                ),
                text=text,
                # hoverinfo='text'
            ))
        else:
            # state_total = 0
            state_mean = filtered_df_cancer[filtered_df_cancer['state'] == state][cancer_stat_name].mean()
            text = state + ':' + str(state_mean) + '<br />'
            cancer_per_state = filtered_df_cancer[filtered_df_cancer['state'] == state].groupby(['cancer_type']).aggregate({cancer_stat_name: 'mean'})[cancer_stat_name]
            for k, v in cancer_per_state.items():
                text = text + cancer_type_mappers.get(k, k) + ': ' + str(v) + '<br />'
                # state_total = state_total + float(v)

            size = 0 if total == 0 else state_mean/total*20

            traces.append(go.Scattermapbox(
                lat=[state_latlong_mappers.get(state)['lat']],
                lon=[state_latlong_mappers.get(state)['lon']],
                mode='markers',
                marker=dict(
                    size=size,
                    color='rgb(255, 0, 0)',
                    # opacity=1.0
                ),
                text=text,
                # hoverinfo='text'
            ))

    return {
        'data': traces,
        'layout': go.Layout(
            autosize=True,
            hovermode='closest',
            showlegend=False,
            mapbox={
                'accesstoken': token,
                'bearing': 0,
                # australia center
                'center': {
                    'lat': -27.2744,
                    'lon': 133.7751
                },
                'pitch': 0,
                'zoom': 3,
                'style': 'light',
            }
        )
    }


def colour_lookup(data):
    return [temp_maps[key] for key in temp_maps if data in key][0]
