import pandas as pd

import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output

import plotly.graph_objs as go

from app import app


df = pd.read_csv('fixtures/cancer/cancer_gender.csv')

# css
colors = {
    'background': '#ffffff',
    'text': '#000000',
    'component': '#BDC3C7',
}
component_style = {
            'width': '95%',
            'padding': '10px 0px 10px 0px',
            'display': 'block',
            'margin': '0 auto'
}

gender_mappers = {
    'M': 'Male',
    'F': 'Female',
    'P': 'Person',
}

cancer_type_mappers = {
    'LIP': 'Lip',
    'MOUTH': 'Mouth',
    'THYROID': 'Thyroid',
    'TONGUE': 'Tongue',
    'OESOPHAGEAL': 'Oesophageal',
}


layout = html.Div(
    style={'backgroundColor': colors['background'], 'width': '100%'},
    children=[
        # cancer stat input
        html.Div(dcc.Dropdown(
            id='cancer-stat',
            options=[
                {'label': 'Incident Count', 'value': 'incident_count'},
                {'label': 'Incident Crude Rate', 'value': 'i_crude_rate'},
                {'label': 'Mortality Count', 'value': 'mortality_count'},
                {'label': 'Mortality Crude Rate', 'value': 'm_crude_rate'},
            ],
            value='incident_count',
            multi=False,
        ), style=component_style),

        # cancer type input
        dcc.Checklist(
            id='cancer-type',
            options=[
                {'label': cancer_type_mappers['LIP'], 'value': 'LIP'},
                {'label': cancer_type_mappers['MOUTH'], 'value': 'MOUTH'},
                {'label': cancer_type_mappers['THYROID'], 'value': 'THYROID'},
                {'label': cancer_type_mappers['TONGUE'], 'value': 'TONGUE'},
                {'label': cancer_type_mappers['OESOPHAGEAL'], 'value': 'OESOPHAGEAL'},
            ],
            values=['LIP', 'MOUTH', 'THYROID', 'TONGUE', 'OESOPHAGEAL'],
            labelStyle={'display': 'inline-block'},
            style=component_style,
        ),
        # Gender Input
        dcc.RadioItems(
            id='gender',
            options=[
                {'label': 'Male', 'value': 'M'},
                {'label': 'Female', 'value': 'F'},
                {'label': 'Person (M+F)', 'value': 'P'},
            ],
            value='P',
            style=component_style,
            labelStyle={'display': 'inline-block'},
        ),

        dcc.Graph(id='cancer-by-gender', style=component_style),
        # year range input
        html.Div(dcc.RangeSlider(
                id='year--slider',
                min=df['year'].min(),
                max=df['year'].max(),
                value=[df['year'].max()-9, df['year'].max()],
                step=None,
                marks={str(year): '{}'.format(year) for year in df['year'].unique()}
            ),
            style=component_style
        )
    ]
)


@app.callback(
    Output('cancer-by-gender', 'figure'),
    [Input('cancer-stat', 'value'),
     Input('cancer-type', 'values'),
     Input('gender', 'value'),
     Input('year--slider', 'value')]
)
def update_figure(cancer_stat_name, cancer_type_values, gender_value, year_value):
    invalid_values = ['..', 'n.p.', 99999.9]
    filtered_df = df[
        (df.year >= year_value[0]) & (df.year <= year_value[1])
        & (df[cancer_stat_name].notnull())
        & (df['gender'] == gender_value)
        & (df['cancer_type'].isin(cancer_type_values))
        & (~df[cancer_stat_name].isin(invalid_values))
    ]

    # convert cancer value into number
    filtered_df[[cancer_stat_name]] = filtered_df[[cancer_stat_name]].astype(float)

    traces = []
    for cancer_type in filtered_df.cancer_type.unique():
        if cancer_stat_name in ('incident_count', 'mortality_count'):
            traces.append(go.Bar(
                    x=filtered_df[filtered_df.cancer_type == cancer_type]['year'],
                    y=filtered_df[filtered_df.cancer_type == cancer_type].groupby('year')[cancer_stat_name].sum(),
                    name=cancer_type_mappers.get(cancer_type, cancer_type),
            ))
        elif cancer_stat_name in ('i_crude_rate', 'm_crude_rate'):
            traces.append(go.Bar(
                    x=filtered_df[filtered_df.cancer_type == cancer_type]['year'],
                    y=filtered_df[filtered_df.cancer_type == cancer_type].groupby('year')[cancer_stat_name].mean(),
                    name=cancer_type_mappers.get(cancer_type, cancer_type),
            ))
    return {
        'data': traces,
        'layout': go.Layout(
            autosize=True,
            showlegend=True,
            margin={'l': 50, 'r': 50, 't': 100, 'b': 100, 'pad': 4},
            legend={'x': 1, 'y': 1},
            barmode='stack',
            title='Cancer Statistics by Gender',
        )
    }
