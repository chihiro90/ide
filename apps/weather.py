"""
Weather Stats Per State - Line Graph.
Allow end-user to select weather state from the dropdown list
and year range from the range slider.
Then show the line graph of selected weather state info per state during the specific selected year range
"""
import dash
import dash_core_components as dcc
import dash_html_components as html

import plotly.graph_objs as go
import pandas as pd

from app import app

colors = {
    'background': '#ffffff',
    'text': '#000000',
    'component': '#BDC3C7',
}
component_style = {
            'width': '90%',
            'padding': '10px 0px 10px 0px',
            'display': 'block',
            'margin': '0 auto'
}

weather_stat_mappers = {
    'tmean': 'Temperature - Mean',
    'tmin': 'Temperature - Min',
    'tmax': 'Temperature - Max',
    'rainfall': 'Rainfall',
    'evaporation': 'Evaporation',
    'cloud': 'Cloud',
}

df = pd.read_csv('fixtures/weather.csv')
df = df[df.year >= 1968]

layout = html.Div(style={'backgroundColor': colors['background']}, children=[
    # define all the input fields including weather stat and year range
    # weather stat input
    html.Div(dcc.Dropdown(
        id='weather-stat',
        options=[
            {'label': weather_stat_mappers['tmean'], 'value': 'tmean'},
            {'label': weather_stat_mappers['tmin'], 'value': 'tmin'},
            {'label': weather_stat_mappers['tmax'], 'value': 'tmax'},
            {'label': weather_stat_mappers['rainfall'], 'value': 'rainfall'},
            {'label': weather_stat_mappers['evaporation'], 'value': 'evaporation'},
            {'label': weather_stat_mappers['cloud'], 'value': 'cloud'},
        ],
        value='tmean',
        multi=False,
    ), style=component_style),
    dcc.Graph(id='weather-per-state', style=component_style),
    # year range input
    html.Div(dcc.RangeSlider(
            id='year--slider',
            min=df['year'].min(),
            max=df['year'].max(),
            value=[df['year'].max()-10, df['year'].max()],
            step=0.5,
            marks={str(year): '{}'.format(year) for year in df['year'].unique()}
        ),
        style=component_style
    ),
])


@app.callback(
   dash.dependencies.Output('weather-per-state', 'figure'),
   [dash.dependencies.Input('weather-stat', 'value'),
    dash.dependencies.Input('year--slider', 'value')])
def update_figure(weather_stat_name, year_value):
    """
    draw one line
    trace1 = go.Scatter(
        x=[1, 2, 3, 4, 5],
        y=[1, 3, 2, 3, 1],
        mode='lines+markers',
        name="'linear'",
        hoverinfo='name',
        line=dict(
            shape='linear'
        )
    )
    """
    filtered_df = df[
        (df.year >= year_value[0]) & (df.year <= year_value[1])
        & (df[weather_stat_name].notnull())
        & (df[weather_stat_name] != 99999.9)
    ]

    traces = []
    for i in filtered_df.state_name.unique():
        traces.append(go.Scatter(
            x=filtered_df[filtered_df['state_name'] == i].sort_values(by=['year']).year.unique(),
            y=filtered_df[filtered_df['state_name'] == i].groupby(['year']).aggregate({weather_stat_name: 'mean'})[weather_stat_name],
            mode='lines+markers',
            line=dict(shape='linear'),
            name=i),
        )
    return {
        'data': traces,
        'layout': go.Layout(
            xaxis={
                'title': 'Year',
            },
            yaxis={
                'title': weather_stat_mappers[weather_stat_name],
            },
            margin={'l': 40, 'b': 40, 't': 100, 'r': 10},
            hovermode='closest',
            legend={'x': 1, 'y': 1},
            title='Weather Statistics by State'
        )
    }
