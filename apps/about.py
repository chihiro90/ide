import dash_html_components as html

layout = html.Div([
    html.Div(
        className="page-header",
        children=[
            html.Br(),
            html.Div('About Intelligent Data Ecosystem', className='page-title'),
        ]
    ),
    html.Div([
        html.Div(
            className="page-information",
            children=[
                html.Br(),
                html.P('Today due to the rapid maturity of machine learning and big data processing communities, heterogeneous sources/types (e.g, text, image,numeric, CSV, etc.) of big data are widely spreading across different research disciplines. One key challenge is how to store and represent such heterogeneous data (e.g., text, numeric, etc.) that can be effectively integrated by state‐of‐the‐art query processing and analysis tools.'),
                html.Br(),
                html.P('Another challenge is how to visualize important properties e.g., to show and correlate the different oral cancer based on geo location, sex and age and weather condition) of such data using different criteria. Such a visualization capability will be able to significantly help with open exploration of the data allowing the discovery of interesting patterns and insights derived from the data.'),
                html.Br(),
                html.P('i‐DE is an interactive dashboard that will provide an intelligent data ecosystem for data storage and visualization, which will help overcome the various challenges.'),
            ],
        ),
    ])
])
