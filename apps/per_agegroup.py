import pandas as pd

import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output

from app import app


df = pd.read_csv('fixtures/cancer/cancer_age_group.csv')

# css
colors = {
    'background': '#ffffff',
    'text': '#000000',
    'component': '#BDC3C7',
}
component_style = {
            'width': '95%',
            'padding': '10px 0px 10px 0px',
            'display': 'block',
            'margin': '0 auto'
}

gender_mappers = {
    'M': 'Male',
    'F': 'Female',
    'P': 'Person',
}

cancer_stat_mappers = {
    'incident_count': 'Incident Count',
    'i_crude_rate': 'Incident Crude Rate',
    'mortality_count': 'Mortality Count',
    'm_crude_rate': 'Mortality Crude Rate',
}

layout = html.Div(style={'backgroundColor': colors['background']}, children=[
    # cancer stat input
    html.Div(dcc.Dropdown(
        id='cancer-stat',
        options=[
            {'label': 'Incident Count', 'value': 'incident_count'},
            {'label': 'Incident Crude Rate', 'value': 'i_crude_rate'},
            {'label': 'Mortality Count', 'value': 'mortality_count'},
            {'label': 'Mortality Crude Rate', 'value': 'm_crude_rate'},
        ],
        value='incident_count',
        multi=False,
    ), style=component_style),

    # cancer type input
    dcc.Checklist(
        id='cancer-type',
        options=[
            {'label': 'Lip', 'value': 'LIP'},
            {'label': 'Mouth', 'value': 'MOUTH'},
            {'label': 'Thyroid', 'value': 'THYROID'},
            {'label': 'Tongue', 'value': 'TONGUE'},
            {'label': 'Oesophageal', 'value': 'OESOPHAGEAL'},
        ],
        values=['LIP', 'MOUTH', 'THYROID', 'TONGUE', 'OESOPHAGEAL'],
        labelStyle={'display': 'inline-block'},
        style=component_style,
    ),
    # Gender Input
    dcc.RadioItems(
        id='gender',
        options=[
            {'label': 'Male', 'value': 'M'},
            {'label': 'Female', 'value': 'F'},
            {'label': 'Person (M+F)', 'value': 'P'},
        ],
        value='P',
        style=component_style,
        labelStyle={'display': 'inline-block'},
    ),
    dcc.Graph(id='cancer-by-age-group', style=component_style),
    # year range input
    html.Div(dcc.RangeSlider(
            id='year--slider',
            min=df['year'].min(),
            max=df['year'].max(),
            value=[df['year'].max()-9, df['year'].max()],
            step=None,
            marks={str(year): '{}'.format(year) for year in df['year'].unique()}
        ), style=component_style,
    ),
])


@app.callback(
    Output('cancer-by-age-group', 'figure'),
    [Input('cancer-stat', 'value'),
     Input('cancer-type', 'values'),
     Input('gender', 'value'),
     Input('year--slider', 'value')]
)
def update_figure(cancer_stat_name, cancer_type_values, gender_value, year_value):
    filtered_df = df[
        (df.year >= year_value[0]) & (df.year <= year_value[1])
        & (df['cancer_stat'] == cancer_stat_name)
        & (df['gender'] == gender_value)
        & (df['cancer_type'].isin(cancer_type_values))]

    labels = [x for x in list(filtered_df) if x not in ('country', 'year', 'cancer_type', 'gender', 'cancer_stat')]
    filtered_df = filtered_df.filter(items=labels)
    values = filtered_df.sum() if cancer_stat_name in ('incident_count', 'mortality_count') else filtered_df.mean()

    return {
        "data": [
            {
                "type": "pie",
                "labels": labels,
                "values": values,
                "textposition": "inside",
                "textinfo": "label+percent",
                "hoverinfo": cancer_stat_mappers[cancer_stat_name] + "value",
                "hole": 0.0,
                'name': 'Test',
            }
        ],
        "layout": {
            "title": "Cancer Statistic per Age Group",
            "autosize": False,
            "height": 550,
        }
    }
