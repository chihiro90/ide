import dash
import dash_core_components as dcc
import dash_html_components as html

import plotly.graph_objs as go
import pandas as pd

from app import app

colors = {
    'background': '#ffffff',
    'text': '#000000',
    'component': '#BDC3C7',
}
component_style = {
            'width': '95%',
            'padding': '10px 0px 10px 0px',
            'display': 'block',
            'margin': '0 auto'
}

df = pd.read_csv('fixtures/cancer/cancer_state.csv')

cancer_stat_mappers = {
    'incident_count': 'Incident Count',
    'i_asr': 'Incident Crude Rate',
    'mortality_count': 'Mortality Count',
    'm_asr': 'Mortality Crude Rate',
}

layout = html.Div(style={'backgroundColor': colors['background']}, children=[
    # cancer stat input
    html.Div(dcc.Dropdown(
        id='cancer-stat',
        options=[
            {'label': cancer_stat_mappers['incident_count'], 'value': 'incident_count'},
            {'label': cancer_stat_mappers['i_asr'], 'value': 'i_asr'},
            {'label': cancer_stat_mappers['mortality_count'], 'value': 'mortality_count'},
            {'label': cancer_stat_mappers['m_asr'], 'value': 'm_asr'},
        ],
        value='incident_count',
        multi=False,
    ), style=component_style),
    # Gender Input
    dcc.RadioItems(
        id='gender',
        options=[
            {'label': 'Male', 'value': 'M'},
            {'label': 'Female', 'value': 'F'},
            {'label': 'Person (M+F)', 'value': 'P'},
        ],
        value='P',
        style=component_style,
        labelStyle={'display': 'inline-block'},
    ),
    # cancer type input
    dcc.Checklist(
        id='cancer-type',
        options=[
            {'label': 'Lip', 'value': 'LIP'},
            {'label': 'Mouth', 'value': 'MOUTH'},
            {'label': 'Thyroid', 'value': 'THYROID'},
            {'label': 'Tongue', 'value': 'TONGUE'},
            {'label': 'Oesophageal', 'value': 'OESOPHAGEAL'},
        ],
        values=['LIP', 'MOUTH', 'THYROID', 'TONGUE', 'OESOPHAGEAL'],
        labelStyle={'display': 'inline-block'},
        style=component_style,
    ),
    dcc.Graph(id='cancer-per-state', style=component_style),
    # year range input
    html.Div(dcc.RangeSlider(
            id='year--slider',
            min=df['year'].min(),
            max=df['year'].max(),
            value=[df['year'].max()-9, df['year'].max()],
            step=None,
            marks={str(year): '{}'.format(year) for year in df['year'].unique()}
        ),
        style=component_style
    ),
])


@app.callback(
   dash.dependencies.Output('cancer-per-state', 'figure'),
   [dash.dependencies.Input('cancer-stat', 'value'),
    dash.dependencies.Input('gender', 'value'),
    dash.dependencies.Input('cancer-type', 'values'),
    dash.dependencies.Input('year--slider', 'value')])
def update_figure(cancer_stat_name, gender_value, cancer_type_values, year_value):
    invalid_values = ['n.p.', '..', ]
    filtered_df = df[
        (df.year >= year_value[0]) & (df.year <= year_value[1])
        & (df[cancer_stat_name].notnull())
        & (~df[cancer_stat_name].isin(invalid_values))
        & (df['cancer_type'].isin(cancer_type_values))
        & (df['gender'] == gender_value)
    ]

    # convert cancer value into number
    filtered_df[[cancer_stat_name]] = filtered_df[[cancer_stat_name]].astype(float)

    traces = []
    aggregate_formula = {cancer_stat_name: 'sum'} if cancer_stat_name in ('incident_count', 'mortality_count') else {cancer_stat_name: 'mean'}
    for i in filtered_df.state.unique():
        traces.append(go.Scatter(
            x=filtered_df[filtered_df['state'] == i]['year'].unique(),
            y=filtered_df[(filtered_df['state'] == i)].groupby(['year']).aggregate(aggregate_formula)[cancer_stat_name],
            mode='lines+markers',
            line=dict(shape='linear'),
            name=i),
        )
    return {
        'data': traces,
        'layout': go.Layout(
            xaxis={
                'title': 'Year',
            },
            yaxis={
                'title': cancer_stat_mappers[cancer_stat_name],
            },
            margin={'l': 40, 'b': 40, 't': 100, 'r': 10},
            hovermode='closest',
            legend={'x': 1, 'y': 1},
            title='Cancer Statistics by State',
        )
    }

